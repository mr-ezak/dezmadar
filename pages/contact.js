import styled from "styled-components"
import Layout from "../components/nobles/MyLayout"
import Link from "next/link"
import { GoBackIcon } from "../components/nobles/Icons"

export default function Contact() {
  return (
    <Layout>
      <ContactWrapper>
        <ContactHeader>ارتباط با ما</ContactHeader>
        <ContactContainer>
          <span>وب سایت ما:</span>
          <p>www.dezmadar.com</p>
          <span>پست الکترونیکی:</span>
          <p>info@dezmadar.com</p>
          <span>تلفن تماس:</span>
          <p>061-42544355</p>
          <span>نشانی:</span>
          <p>دزفول ، خیابان پیام آوران نبش آفرینش مجتمع تجاری تفریحی مادر</p>
        </ContactContainer>
      </ContactWrapper>
    </Layout>
  )
}

const ContactWrapper = styled.div`
  width: 100%;
  height: auto;
  direction: rtl;
  overflow: hidden;
`

const ContactHeader = styled.h2`
  width: 100%;
  height: 50px;
  line-height: 50px;
  direction: rtl;
  margin: 0;
  float: right;
  color: #b05082;
  text-align: center;
`

const ContactContainer = styled.div`
  width: 85%;
  height: auto;
  margin: 50px auto;
  padding: 10px;
  background: #fff;
  border-radius: 5px;
  box-shadow: 0px 0px 5px #999;
  span {
    color: #333;
    font-size: 17px;
  }
  p {
    color: #333;
    font-size: 14px;
    width: 95%;
    text-align: justify;
  }
`
