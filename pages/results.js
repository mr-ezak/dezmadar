import * as React from "react"
import styled from "styled-components"
import axios from "axios"
import Link from "next/link"
import NumberFormat from "react-number-format"
import { Loading } from "../components/nobles/loading"
import {
  ViewIcon,
  PlusIcon,
  SearchIcon,
  GoBackIcon
} from "../components/nobles/Icons"

export default function Results(props) {
  const [keyWrited, setKeyWrited] = React.useState("")
  const [products, setProducts] = React.useState([])

  React.useEffect(() => {
    getProducts()
  }, [])
  React.useEffect(() => {
    fullSearch()
  }, [keyWrited])

  const fullSearch = async () => {
    const writed = keyWrited
    if (writed != "") {
      const response = await axios.get(
        "https://base.bamadar.com/api/get_products?name=" + writed
      )
      if (response.data.data) {
        setProducts(response.data.data)
        window.localStorage.removeItem("searchWord")
      }
    }
  }

  const getProducts = async () => {
    if (window.localStorage.getItem("searchWord")) {
      const word = window.localStorage.getItem("searchWord")
      const response = await axios.get(
        "https://base.bamadar.com/api/get_products?name=" + word
      )
      if (response.data.data) {
        setProducts(response.data.data)
        window.localStorage.removeItem("searchWord")
      }
    }
  }

  const addToCart = data => {
    if (window.localStorage.getItem("MadarCart")) {
      let MadarArr = JSON.parse(localStorage.getItem("MadarCart"))
      let NewMadarArray = []
      let AddNew = true
      MadarArr.forEach((element, i) => {
        let pro = { ...element }
        if (element.id == data.id) {
          pro.balance = element.balance + 1
          AddNew = false
        }
        // console.log(pro)
        NewMadarArray.push(pro)
      })
      if (AddNew == true) {
        const pro = {
          id: data.id,
          balance: 1,
          name: data.name,
          price: data.sell_price
        }
        NewMadarArray.push(pro)
      }
      // console.log(NewMadarArray)
      window.localStorage.setItem("MadarCart", JSON.stringify(NewMadarArray))
    } else {
      const pro = {
        id: data.id,
        balance: 1,
        name: data.name,
        price: data.sell_price
      }
      const MadarArr = []
      MadarArr.push(pro)
      console.log(MadarArr)
      window.localStorage.setItem("MadarCart", JSON.stringify(MadarArr))
    }
  }

  return (
    <>
      {products == [] ? (
        <Loading />
      ) : (
        <ResultsWrapper>
          <ResultsHeader>جستجوی محصول</ResultsHeader>
          <ResultsReturn>
            <Link href='/'>
              <GoBackIcon />
            </Link>
          </ResultsReturn>
          <ResultsSearchBox>
            <SearchIcon onClick={() => fullSearch()} />
            <input
              type='text'
              name='searchBox'
              placeholder='جستجو محصول'
              onChange={event => {
                setKeyWrited(event.target.value)
              }}
            />
          </ResultsSearchBox>
          <ResultsContainer>
            <ul>
              {products.map((data, index) => (
                <li key={index}>
                  <img src='/static/images/Logo.png'></img>
                  <span name='values'>
                    <p>{data.name}</p>
                    <NumberFormat
                      value={data.sell_price}
                      displayType={"text"}
                      thousandSeparator={true}
                      prefix={"ریال"}
                    />
                  </span>
                  <ResultsProAdd onClick={() => addToCart(data)}>
                    <PlusIcon />
                  </ResultsProAdd>
                  <ResultsProView>
                    <ViewIcon />
                  </ResultsProView>
                </li>
              ))}
            </ul>
          </ResultsContainer>
        </ResultsWrapper>
      )}
    </>
  )
}

const ResultsWrapper = styled.div`
  width: 100%;
  height: auto;
  overflow: hidden;
`

const ResultsHeader = styled.h3`
  width: 50%;
  height: 50px;
  line-height: 50px;
  direction: rtl;
  margin: 0px auto;
  color: #b05082;
  text-align: center;
`
const ResultsSearchBox = styled.div`
  width: 95%;
  height: 40px;
  background: #eceff4;
  border-radius: 5px;
  box-shadow: 0px 0px 5px #999;
  margin: 0px auto;
  position: relative;
  input[name="searchBox"] {
    width: 80%;
    height: 30px;
    border: medium none;
    outline: medium none;
    padding: 0;
    padding-right: 5px;
    margin: 0;
    margin-top: 5px;
    margin-right: 5px;
    background: #eceff4;
    border-radius: 10px;
    direction: rtl;
    font-size: 11px;
    font-family: BYekan;
  }
  svg {
    float: right;
    margin-top: 10px;
  }
`
const ResultsReturn = styled.div`
  width: 40px;
  height: 40px;
  position: absolute;
  top: 15px;
  left: 10px;
  cursor: pointer;
`

const ResultsContainer = styled.div`
  width: 100%;
  height: auto;
  min-height: 50px;
  margin-top: 10px;
  overflow: hidden;
  direction: rtl;
  transition: all 600ms;

  form {
    height: auto;
  }
  input {
    width: 200px;
    height: 30px;
    display: block;
    margin-right: 20px;
    margin-top: 3px;
    background: none;
    padding-right: 5px;
    border-radius: 3px;
    color: #333;
  }
  input::placeholder {
    color: #333;
  }
  textarea {
    width: 200px;
    height: 200px;
    display: block;
    margin-right: 20px;
    margin-top: 3px;
    background: none;
    padding-right: 5px;
    border-radius: 3px;
    color: #333;
    resize: none;
  }
  ul {
    width: 95%;
    height: auto;
    list-style: none;
    margin-right: 10px;
    margin-top: -15px;
    margin-bottom: 10px;
    padding-right: 10px;
  }
  li {
    width: 95%;
    height: 70px;
    margin-top: 20px;
    padding: 5px;
    font-size: 12px;
    background: #fff;
    border-radius: 10px;
    box-shadow: 0px 0px 5px #999;
    position: relative;
    img {
      display: block;
      width: 60px;
      hight: 60px;
      margin-top: 2px;
      margin-right: 2px;
      float: right;
    }
    span[name="values"] {
      display: block;
      float: right;
      margin-right: 7px;
      margin-top: -5px;
      width: 65%;
      height: 70px;
      p {
        width: 100%;
        margin-top: 10px;
        margin-right: 5px;
      }
      p:nth-child(1) {
        font-weight: bold;
        font-size: 11px;
      }
    }
  }
`
const ResultsBtnCon = styled.div`
  width: 70px;
  height: 70px;
  float: left;
`
const ResultsProView = styled.div`
  width: 30px;
  height: 30px;
  float: right;
  background: #ee9827;
  margin-top: 2px;
  margin-right: 5px;
  border-radius: 7px;
  cursor: pointer;
  svg {
    margin-top: 6px;
    margin-left: 6px;
  }
`

const ResultsProAdd = styled.div`
  width: 30px;
  height: 30px;
  float: right;
  background: #3498db;
  margin-right: 5px;
  border-radius: 7px;
  cursor: pointer;
  svg {
    margin-top: 3px;
    margin-left: 3px;
  }
`

// const ResultsAddBTN = styled.button`
//   width: 120px;
//   height: 40px;
//   background-color: #00897b;
//   border-radius: 5px;
//   border: none;
//   margin-top: 20px !important;
//   margin-right: 20px;
//   margin-bottom: 20px;
//   text-align: center;
//   line-height: 31px;
//   color: white;
//   float: right;
//   font-family: BYekan;
// `
const ResultsSingleBTN = styled.button`
  width: 110px;
  height: 25px;
  background: #eac100;
  border-radius: 15px;
  float: left;
  font-family: BYekan;
  font-size: 10px;
  border: none;
  color: #fff;
  cursor: pointer;
  position: absolute;
  bottom: 5px;
  left: 0;
  right: 0;
  margin-right: auto;
  margin-left: auto;
`
const ResultsSingleContainer = styled.div`
  width: 90%;
  height: auto;
  margin: 0px auto;
  direction: rtl;
  font-size: 14px;
  div[class="service-single-btn-container"] {
    width: 100%;
    height: 80px;
  }
`
const ResultsSingleTitle = styled.div`
  width: 100%;
  height: 40px;
  display: block;
  margin-top: 20px;
`
const ResultsSingleDes = styled.div`
  width: 100%;
  height: 95px;
  display: block;
  margin-top: 20px;
`
const ResultsSinglePrice = styled.div`
  width: 100%;
  height: 40px;
  display: block;
  margin-top: 20px;
`
const ResultsSingleReqBTN = styled.div`
  width: 100px;
  height: 35px;
  line-height: 35px;
  text-align: center;
  background: #eac100;
  border-radius: 5px;
  margin-top: 10px;
  color: #fff;
  cursor: pointer;
`
const ResultsSingleCancelBTN = styled.div`
  width: 96px;
  height: 31px;
  line-height: 31px;
  text-align: center;
  background: #fff;
  border: 2px solid #eac100;
  color: #eac100;
  border-radius: 5px;
  margin-top: 10px;
  cursor: pointer;
`
const ResultsMsgBox = styled.div`
  width: 200px;
  height: 40px;
  background: ${p => (p.msgcolor ? p.msgcolor : "#fff")};
  display: ${d => (d.msgstatus ? d.msgstatus : "block")};
  margin: 0px auto;
  text-align: center;
  float: right;
  color: #fff;
  border-radius: 5px;
`
