import styled from "styled-components"
import Layout from "../components/nobles/MyLayout"
import Link from "next/link"
import { GoBackIcon } from "../components/nobles/Icons"

export default function About() {
  return (
    <Layout>
      <AboutWrapper>
        <AboutHeader>درباره ما</AboutHeader>
        <AboutContainer>
          <p>
            شركت “پويا سازه دورانتاش” در عرصه ساخت مسكن و كارهاي عمراني با نام
            تجاری “مسكن سازان پويا” در شهرستان دزفول مشغول به فعاليت مي‌باشد و
            هم اکنون در قالب ۸۳ پروژه ساختماني در بهترين نقاط ، بيش از ۱۲۰۰ واحد
            آپارتماني را در حال ساخت دارد كه بالغ بر ۷۰۰ واحد آپارتماني را تحويل
            متقاضیان داده است و مابقی پروژه‌هایی که شرکت تعهد فروش از آنها دارد
            بالغ بر ۶۰ درصد پیشرفت فیزیکی دارند.
          </p>
          <h4> - هایپر مارکت مادر</h4>
          <p>
            بزرگترین هایپر مارکت جنوب غرب کشور با زیر بنایی بالغ بر 6000 متر
            مربع
          </p>
          <h4> - واحد های تجاری</h4>
          <p>
            ۳۴۳ واحد تجاری در طبقات منفی 1 . همکف ، اول و دوم از متراژ 10 الی
            400مترمربع
          </p>
          <h4> - رستوران سنتی</h4>
          <p>رستوران سنتی و بین المللی در مجاورت و مشرف به باغ بام</p>
          <h4> - شهربازی سرپوشیده مدرن</h4>
          <p>
            با امکانات متنوع بازی و در مساحت حدود 2000متر مربع ، استخر ، سونا و
            جکوزی در طبقه منفی{" "}
          </p>
          <h4> - جاذبه‌های تفریحی و گردشگری</h4>
          <p>
            شامل: باغ بام، شبیه سازی آبشار شوی، آب‌نمای موزیکال، آسیاب آبی و
            مدل‌سازی بافت قدیم شهر دزفول
          </p>
          <h4> - باشگاه ورزشی ویژه بانوان</h4>
          <p>
            مرتبط با استخر به همراه آسانسور اختصاصی در دو طبقه فست فود در طبقه
            دوم
          </p>
          <h4> - شهر کودک</h4>
          <p>
            ( فضاهای اختصاصی کودکان به همراه سالن ویژه برگزاری جشن تولد کودک و
            زبانکده کودک ) در طبقه سوم
          </p>
          <h4> - سالن بولینگ</h4>
          <p>سالن بولینگ، بیلیارد</p>
        </AboutContainer>
      </AboutWrapper>
    </Layout>
  )
}

const AboutWrapper = styled.div`
  width: 100%;
  height: auto;
  direction: rtl;
  overflow: hidden;
`
const AboutHeader = styled.h2`
  width: 100%;
  height: 50px;
  line-height: 50px;
  direction: rtl;
  margin: 0;
  float: right;
  color: #b05082;
  text-align: center;
`
const AboutContainer = styled.div`
  width: 85%;
  height: auto;
  margin: 50px auto;
  background: #fff;
  box-shadow: 0px 0px 5px #999;
  border-radius: 5px;
  padding: 10px;
  p {
    width: 95%;
    text-align: justify;
    font-size: 14px;
    margin-top: 5px;
  }
  p:nth-child(1) {
    margin-top: 10px;
  }
  h4 {
    margin-top: 10px;
  }
  ul {
    padding-right: 20px;
    font-size: 14px;
  }
`
