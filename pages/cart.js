import styled from "styled-components"
import Layout from "../components/nobles/MyLayout"
import { Component } from "react"
import Link from "next/link"
import NumberFormat from "react-number-format"
import { MultiplyIcon } from "../components/nobles/Icons"

export default function Cart(props) {
  const [logged, setLogged] = React.useState(1)
  const [products, setProducts] = React.useState([])
  const [allProPrice, setAllProPrice] = React.useState(0)
  const [allProCount, setAllProCount] = React.useState(0)

  React.useEffect(() => {
    getCart()
  }, [])

  const getCart = () => {
    if (window.localStorage.getItem("MadarCart")) {
      let MadarArr = JSON.parse(localStorage.getItem("MadarCart"))
      let NewMadarArr = []
      MadarArr.map((data, index) => {
        const allPrice = parseInt(data.price) * parseInt(data.balance)
        const pro = {
          id: data.id,
          balance: data.balance,
          name: data.name,
          price: data.price,
          allPrice: allPrice
        }
        NewMadarArr.push(pro)
      })

      //Count the Products
      let allCount = []
      let allPrices = []
      NewMadarArr.map((data, index) => {
        allCount.push(data.balance)
      })
      NewMadarArr.map((data, index) => {
        allPrices.push(data.allPrice)
      })

      setAllProCount(
        allCount.reduce(function(acc, val) {
          return acc + val
        }, 0)
      )
      setAllProPrice(
        allPrices.reduce(function(acc, val) {
          return acc + val
        }, 0)
      )

      setProducts(NewMadarArr)
    }
  }
  const removeCartItem = data => {
    //Geting the Array from localStorage
    let MadarArr = JSON.parse(localStorage.getItem("MadarCart"))

    //Finding it with giving items id to findIndex to find its index
    const index = MadarArr.findIndex(x => x.id === data.id)

    //Remove the Item from Array
    MadarArr.splice(index, 1)

    //Push back the new array to local Storage
    window.localStorage.setItem("MadarCart", JSON.stringify(MadarArr))
    getCart()
  }

  return (
    <>
      <Layout>
        {logged == 1 ? (
          <CartWrapper>
            <CartHeader>سبد خرید</CartHeader>
            <CartContainer>
              <ul>
                {products.map((data, index) => (
                  <li key={index}>
                    <img src='/static/images/Logo.png'></img>
                    <span name='prodata'>
                      <p>{data.name}</p>
                      <span name='singlePrice'>
                        <span name='PriceTitle'>قیمت تک:‌</span>
                        <span>
                          <NumberFormat
                            value={data.price}
                            displayType={"text"}
                            thousandSeparator={true}
                            prefix={"ریال"}
                          />
                        </span>
                      </span>
                      <span name='allPrice'>
                        <span name='PriceTitle'>قیمت کل :</span>
                        <span>
                          <NumberFormat
                            value={data.allPrice}
                            displayType={"text"}
                            thousandSeparator={true}
                            prefix={"ریال"}
                          />
                        </span>
                      </span>
                      <span name='count'>
                        <span name='PriceTitle'>قیمت کل :</span>
                        <span>{data.balance}</span>
                      </span>
                    </span>
                    <CartProDelete onClick={() => removeCartItem(data)}>
                      <MultiplyIcon />
                    </CartProDelete>
                  </li>
                ))}
              </ul>
            </CartContainer>
            <Cartresutls>
              <span>مبلغ نهایی :‌ </span>
              <span name='value'>
                <NumberFormat
                  value={allProPrice}
                  displayType={"text"}
                  thousandSeparator={true}
                  prefix={"ریال"}
                />
              </span>
              <span>تعداد : </span>
              <span name='value'>{allProCount}</span>
            </Cartresutls>
            <Link href='/sellInfo'>
              <CartGoBtn>
                <span>نهایی کردن سبد خرید</span>
              </CartGoBtn>
            </Link>
          </CartWrapper>
        ) : (
          <Link href='/login'>
            <a>
              <span>ورود به نرم افزار</span>
            </a>
          </Link>
        )}
      </Layout>
    </>
  )
}

const CartWrapper = styled.div`
  width: 100%;
  height: auto;
  overflow: hidden;
  background: #eceff4;
`

const CartHeader = styled.h3`
  width: 100%;
  height: 50px;
  line-height: 50px;
  direction: rtl;
  margin: 0;
  float: right;
  color: #b05082;
  text-align: center;
`

const CartContainer = styled.div`
  width: 100%;
  height: auto;
  min-height: 50px;
  margin-top: 10px;
  overflow: hidden;
  direction: rtl;
  transition: all 600ms;

  form {
    height: auto;
  }
  input {
    width: 200px;
    height: 30px;
    display: block;
    margin-right: 20px;
    margin-top: 3px;
    background: none;
    padding-right: 5px;
    border-radius: 3px;
    color: #333;
  }
  input::placeholder {
    color: #333;
  }
  textarea {
    width: 200px;
    height: 200px;
    display: block;
    margin-right: 20px;
    margin-top: 3px;
    background: none;
    padding-right: 5px;
    border-radius: 3px;
    color: #333;
    resize: none;
  }
  ul {
    width: 95%;
    height: auto;
    list-style: none;
    margin-right: 10px;
    margin-top: -15px;
    margin-bottom: 10px;
    padding-right: 10px;
  }
  li {
    width: 95%;
    height: 120px;
    margin-top: 20px;
    padding: 5px;
    font-size: 12px;
    background: #fff;
    border-radius: 10px;
    box-shadow: 0px 0px 5px #999;
    position: relative;
    img {
      display: block;
      width: 60px;
      hight: 60px;
      margin-top: 2px;
      margin-right: 2px;
      float: right;
    }
    span[name="prodata"] {
      display: block;
      float: right;
      margin-right: 7px;
      margin-top: -5px;
      width: 62%;
      height: 70px;
      p {
        width: 100%;
        margin-top: 10px;
        margin-right: 5px;
      }
      p:nth-child(1) {
        font-weight: bold;
        font-size: 12px;
      }
    }
    span[name="singlePrice"] {
      display: block;
      padding-right: 5px;
      margin-top: 3px;
    }
    span[name="allPrice"] {
      display: block;
      padding-right: 5px;
      margin-top: 3px;
    }
    span[name="count"] {
      display: block;
      padding-right: 5px;
      margin-top: 3px;
    }
  }
`
const CartProDelete = styled.div`
  width: 35px;
  height: 35px;
  float: right;
  background: #e74c3c;
  margin-top: 5px;
  margin-right: 5px;
  border-radius: 7px;
  cursor: pointer;
  svg {
    margin-top: 12px;
    margin-left: 12px;
  }
`
const Cartresutls = styled.div`
  width: 80%;
  height: 100px;
  margin: 10px auto;
  border-radius: 5px;
  border: 1px dashed #999;
  text-align: center;
  span {
    display: block;
  }
  span[name="value"] {
    color: #666;
  }
`
const CartGoBtn = styled.div`
  width: 150px;
  height: 36px;
  line-height: 36px;
  border-radius: 5px;
  text-align: center;
  color: #efefef;
  font-size: 14px;
  margin: 0px auto;
  background: #3498db;
  cursor: pointer;
`
const CartMsgBox = styled.div`
  width: 200px;
  height: 40px;
  background: ${p => (p.msgcolor ? p.msgcolor : "#fff")};
  display: ${d => (d.msgstatus ? d.msgstatus : "block")};
  margin: 0px auto;
  text-align: center;
  float: right;
  color: #fff;
  border-radius: 5px;
`
