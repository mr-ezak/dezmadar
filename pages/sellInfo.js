import styled from "styled-components"
import Layout from "../components/nobles/MyLayout"
import Link from "next/link"

export default function SellInfo(props) {
  const [logged, setLogged] = React.useState(1)

  return (
    <>
      <Layout>
        {logged == 1 ? (
          <SellInfoWrapper>
            <SellInfoHeader>تکمیل اطلاعات خریدار</SellInfoHeader>
            <SellInfoContainer></SellInfoContainer>
          </SellInfoWrapper>
        ) : (
          <Link href='/login'>
            <a>
              <span>ورود به نرم افزار</span>
            </a>
          </Link>
        )}
      </Layout>
    </>
  )
}

const SellInfoWrapper = styled.div`
  width: 100%;
  height: auto;
  overflow: hidden;
  background: #eceff4;
`

const SellInfoHeader = styled.h3`
  width: 100%;
  height: 50px;
  line-height: 50px;
  direction: rtl;
  margin: 0;
  float: right;
  color: #b05082;
  text-align: center;
`

const SellInfoContainer = styled.div`
  width: 100%;
  height: auto;
  min-height: 50px;
  margin-top: 10px;
  overflow: hidden;
  direction: rtl;
  transition: all 600ms;

  form {
    height: auto;
  }
  input {
    width: 200px;
    height: 30px;
    display: block;
    margin-right: 20px;
    margin-top: 3px;
    background: none;
    padding-right: 5px;
    border-radius: 3px;
    color: #333;
  }
  input::placeholder {
    color: #333;
  }
  textarea {
    width: 200px;
    height: 200px;
    display: block;
    margin-right: 20px;
    margin-top: 3px;
    background: none;
    padding-right: 5px;
    border-radius: 3px;
    color: #333;
    resize: none;
  }
`
