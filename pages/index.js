import React, { Component } from "react"
import Layout from "../components/nobles/MyLayout"
import { Loading } from "../components/nobles/loading"
import { Slider1 } from "../components/dashboard/slider1"
import { Sections } from "../components/dashboard/sections"
import Login from "../components/nobles/login"

export default class Index extends Component {
  state = {
    logged: 1
  }

  // componentDidMount() {
  //   this.loginCheck()
  // }

  render = () => (
    <>
      {this.state.logged == 0 ? (
        <Login />
      ) : (
        <Layout>
          <div>
            <Slider1 />
            <Sections />
          </div>
        </Layout>
      )}
    </>
  )
}
