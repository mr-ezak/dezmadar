import React, { Component } from "react"
import styled from "styled-components"
import { postData } from "../utils/main"
import Layout from "../components/nobles/MyLayout"
import { Sections } from "../components/dashboard/sections"
import Router from "next/router"

export default function Categories() {
  return (
    <Layout>
      <div>
        <CatTitle>دسته بندی محصولات</CatTitle>
        <Sections />
      </div>
    </Layout>
  )
}

const CatTitle = styled.h3`
  width: 100%;
  height: 50px;
  line-height: 50px;
  direction: rtl;
  margin: 0;
  float: right;
  color: #b05082;
  text-align: center;
`
