import Router from "next/router"

// import request from "request"
export function postData(url = "", data = {}) {
  return fetch(url, {
    method: "POST", // *GET, POST, PUT, DELETE, etc.
    body: JSON.stringify(data), // body data type must match "Content-Type" header
    headers: {
      Accept: "application/json"
      // 'Content-Type': 'application/x-www-form-urlencoded',
    }
  }).then(response => response.json()) // parses JSON response into native Javascript objects
}

export function loginCheck() {
  if (typeof window == "object") {
    if (window.localStorage.getItem("usertoken") === null) {
      Router.push("/landing")
    } else {
      const userTk = window.localStorage.getItem("usertoken")
      postData(
        "https://api.salomba.ir/update.php?action=tokencheck&token=" + userTk
      ).then(data => {
        if (data == "good") {
          return 1
        } else {
          Router.push("/landing")
        }
      })
    }
  }
}

export function logout() {
  if (typeof window == "object") {
    if (window.localStorage.getItem("usertoken") === null) {
      Router.push("/landing")
    } else {
      window.localStorage.removeItem("usertoken")
      Router.push("/landing")
    }
  }
}
