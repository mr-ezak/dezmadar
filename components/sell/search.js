import styled from "styled-components"
import Layout from "../components/nobles/MyLayout"
import { Component } from "react"
import Link from "next/link"
import { postData, loginCheck } from "../utils/main"
import { ViewIcon, PlusIcon } from "../components/nobles/Icons"

export default class Search extends Component {
  state = {
    logged: 1,
    keytoken: "0",
    products: [],
    userId: null
  }

  render = () => (
    <>
      <Layout>
        {this.state.logged == 1 ? (
          <SearchWrapper>
            <SearchHeader>جستجوی محصول</SearchHeader>
            <SearchContainer>
              <ul>
                {/* {this.state.Search.map((data, index) => (
                  <li key={index}>
                    {data.des}{" "}
                    <SearchSingleBTN onClick={() => this.SearchingleGo(data.id)}>
                      درخواست
                    </SearchSingleBTN>
                  </li>
                ))} */}
                <li>
                  <img src='/static/images/Logo.png'></img>
                  <span>
                    <p>نام کامل محصول</p>
                    <p>200000 ریال</p>
                  </span>
                  <SearchProAdd>
                    <PlusIcon />
                  </SearchProAdd>
                  <SearchProView>
                    <ViewIcon />
                  </SearchProView>
                </li>
              </ul>
            </SearchContainer>
          </SearchWrapper>
        ) : (
          <Link href='/login'>
            <a>
              <span>ورود به نرم افزار</span>
            </a>
          </Link>
        )}
      </Layout>
    </>
  )
}

const SearchWrapper = styled.div`
  width: 100%;
  height: auto;
  overflow: hidden;
  background: #eceff4;
`

const SearchHeader = styled.h3`
  width: 100%;
  height: 50px;
  line-height: 50px;
  direction: rtl;
  margin: 0;
  float: right;
  color: #b05082;
  text-align: center;
`

const SearchContainer = styled.div`
  width: 100%;
  height: auto;
  min-height: 50px;
  margin-top: 10px;
  overflow: hidden;
  direction: rtl;
  transition: all 600ms;

  form {
    height: auto;
  }
  input {
    width: 200px;
    height: 30px;
    display: block;
    margin-right: 20px;
    margin-top: 3px;
    background: none;
    padding-right: 5px;
    border-radius: 3px;
    color: #333;
  }
  input::placeholder {
    color: #333;
  }
  textarea {
    width: 200px;
    height: 200px;
    display: block;
    margin-right: 20px;
    margin-top: 3px;
    background: none;
    padding-right: 5px;
    border-radius: 3px;
    color: #333;
    resize: none;
  }
  ul {
    width: 95%;
    height: auto;
    list-style: none;
    margin-right: 10px;
    margin-top: -15px;
    margin-bottom: 10px;
    padding-right: 10px;
  }
  li {
    width: 95%;
    height: 70px;
    margin-top: 20px;
    padding: 5px;
    font-size: 12px;
    background: #fff;
    border-radius: 10px;
    box-shadow: 0px 0px 5px #999;
    position: relative;
    img {
      display: block;
      width: 60px;
      hight: 60px;
      margin-top: 2px;
      margin-right: 2px;
      float: right;
    }
    span {
      display: block;
      float: right;
      margin-right: 7px;
      margin-top: -5px;
      width: 65%;
      height: 70px;
      p {
        width: 100%;
        margin-top: 10px;
        margin-right: 5px;
      }
      p:nth-child(1) {
        font-weight: bold;
        font-size: 11px;
      }
    }
  }
`
const SearchBtnCon = styled.div`
  width: 70px;
  height: 70px;
  float: left;
`
const SearchProView = styled.div`
  width: 30px;
  height: 30px;
  float: right;
  background: #ee9827;
  margin-top: 2px;
  margin-right: 5px;
  border-radius: 7px;
  cursor: pointer;
  svg {
    margin-top: 6px;
    margin-left: 6px;
  }
`

const SearchProAdd = styled.div`
  width: 30px;
  height: 30px;
  float: right;
  background: #3498db;
  margin-right: 5px;
  border-radius: 7px;
  cursor: pointer;
  svg {
    margin-top: 3px;
    margin-left: 3px;
  }
`

// const SearchAddBTN = styled.button`
//   width: 120px;
//   height: 40px;
//   background-color: #00897b;
//   border-radius: 5px;
//   border: none;
//   margin-top: 20px !important;
//   margin-right: 20px;
//   margin-bottom: 20px;
//   text-align: center;
//   line-height: 31px;
//   color: white;
//   float: right;
//   font-family: BYekan;
// `
const SearchSingleBTN = styled.button`
  width: 110px;
  height: 25px;
  background: #eac100;
  border-radius: 15px;
  float: left;
  font-family: BYekan;
  font-size: 10px;
  border: none;
  color: #fff;
  cursor: pointer;
  position: absolute;
  bottom: 5px;
  left: 0;
  right: 0;
  margin-right: auto;
  margin-left: auto;
`
const SearchSingleContainer = styled.div`
  width: 90%;
  height: auto;
  margin: 0px auto;
  direction: rtl;
  font-size: 14px;
  div[class="service-single-btn-container"] {
    width: 100%;
    height: 80px;
  }
`
const SearchSingleTitle = styled.div`
  width: 100%;
  height: 40px;
  display: block;
  margin-top: 20px;
`
const SearchSingleDes = styled.div`
  width: 100%;
  height: 95px;
  display: block;
  margin-top: 20px;
`
const SearchSinglePrice = styled.div`
  width: 100%;
  height: 40px;
  display: block;
  margin-top: 20px;
`
const SearchSingleReqBTN = styled.div`
  width: 100px;
  height: 35px;
  line-height: 35px;
  text-align: center;
  background: #eac100;
  border-radius: 5px;
  margin-top: 10px;
  color: #fff;
  cursor: pointer;
`
const SearchSingleCancelBTN = styled.div`
  width: 96px;
  height: 31px;
  line-height: 31px;
  text-align: center;
  background: #fff;
  border: 2px solid #eac100;
  color: #eac100;
  border-radius: 5px;
  margin-top: 10px;
  cursor: pointer;
`
const SearchMsgBox = styled.div`
  width: 200px;
  height: 40px;
  background: ${p => (p.msgcolor ? p.msgcolor : "#fff")};
  display: ${d => (d.msgstatus ? d.msgstatus : "block")};
  margin: 0px auto;
  text-align: center;
  float: right;
  color: #fff;
  border-radius: 5px;
`
