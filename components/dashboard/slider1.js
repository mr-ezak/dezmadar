import React, { Component } from "react"
import styled from "styled-components"
import dynamic from "next/dynamic"
// const Flickity = dynamic(import("react-flickity-component"), { ssr: false })
import { SliderNextArrow, SliderPrevArrow } from "../nobles/Icons"

const flickityOptions = {
  cellAlign: "center",
  pageDots: false,
  draggable: true,
  prevNextButtons: false,
  pageDots: false,
  freeScroll: false,
  autoPlay: true,
  autoPlay: 1500,
  cellAlign: "center"
}

export class Slider1 extends Component {
  state = {
    slides: [
      "/static/images/slider/1.jpg",
      "/static/images/slider/2.jpg",
      "/static/images/slider/3.jpg"
    ],
    scrollSize: 0
  }

  constructor(props) {
    super(props)
    this.slider = React.createRef()
  }

  handleSlider = action => {
    if (action == "prev") {
      const toGo = this.slider.current.scrollLeft - 300
      this.slider.current.scrollTo({
        left: toGo,
        behavior: "smooth"
      })
    } else if (action == "next") {
      const toGo = this.slider.current.scrollLeft + 300
      this.slider.current.scrollTo({
        left: toGo,
        behavior: "smooth"
      })
    }
  }

  componentDidMount() {}

  render = () => (
    <SliderWrapper>
      <img src='/static/images/slider/3.jpeg' />
    </SliderWrapper>
  )
}

/* - - - Page Components - - - */

const SliderWrapper = styled.div`
  width: 100%;
  height: 230px;
  position: relative;
  div[class="slider-body"] {
    width: 100%;
    height: 180px;
    overflow: hidden;
    padding: 5px 0px;
  }
  img {
    display: block;
    width: 100%;
    max-width: 500px;
    height: 230px;
    margin: 0px auto;
  }
`

const SliderBody = styled.div`
  width: 92%;
  height: 180px;
  box-shadow: 0px 0px 5px #333;
  color: white;
  text-align: center;
  margin: 10px auto;
  display: flex;
  scroll-snap-type: x mandatory;
  overflow-x: scroll;
  overflow: hidden;
  border-radius: 10px;
`
const SliderItem = styled.div`
  width: 90%;
  height: 180px;
  scroll-snap-align: start;
  border-radius: 10px;
  overflow: hidden;
  margin-right: 10px;
  margin-left: 10px;
  box-shadow: 0px 0px 5px #333;
  border-radius: 5px;
`
const SliderImage = styled.img`
  display: block;
  background-size: cover;
  width: 100%;
  height: 100%;
  border-radius: 5px;
`
const SliderPrev = styled.div`
  width: 20px;
  height: 20px;
  cursor: pointer;
  border-radius: 10px;
  position: absolute;
  top: 120px;
  left: 15px;
  background: rgba(255, 255, 255, 0.5);
  transition: all 200ms;

  svg {
    display: block;
    width: 100%;
    height: 100%;
    margin: 5px auto;
    margin-left: 5px;
  }

  :hover {
    transform: translateX(2px);
  }
`

const SliderNext = styled.div`
  width: 20px;
  height: 20px;
  cursor: pointer;
  border-radius: 10px;
  position: absolute;
  top: 120px;
  right: 15px;
  background: rgba(255, 255, 255, 0.5);
  transition: all 200ms;

  svg {
    display: block;
    width: 100%;
    height: 100%;
    margin: 5px auto;
    margin-left: 8px;
  }

  :hover {
    transform: translateX(-2px);
  }
`
const CreditBox = styled.div`
  width: 140px;
  height: 30px;
  background: #00ad7c;
  border-radius: 15px;
  margin: 10px auto;
  span[class="credilable1"] {
    width: 30px;
    height: 30px;
    line-height: 30px;
    display: block;
    float: right;
    color: #fff;
    direction: rtl;
    margin-right: 10px;
    font-size: 13px;
  }
  p {
    width: 30px;
    height: 30px;
    margin: 0px auto;
    margin-right: 10px;
    line-height: 29px;
    float: right;
    font-family: BYekan;
    color: #fff;
  }
  span[class="credilable2"] {
    width: 30px;
    height: 30px;
    line-height: 30px;
    display: block;
    float: left;
    color: #fff;
    direction: rtl;
    margin-left: 10px;
    font-size: 13px;
  }
`
