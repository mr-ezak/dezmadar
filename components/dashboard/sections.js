import React, { Component } from "react"
import styled from "styled-components"
import Link from "next/dist/client/link"

export class Sections extends Component {
  state = {}

  render = () => (
    <SectionWrapper>
      {/* <ServTitle>خدمات ما</ServTitle> */}
      <SectionBody>
        <div class='boxes'>
          <a>
            <img src='../../static/images/sections/LOGO-12.png'></img>
            <span>موادغذایی</span>
          </a>
          <a>
            <img src='../../static/images/sections/LOGO-08.png'></img>
            <span>بهداشتی</span>
          </a>
          <a>
            <img src='../../static/images/sections/LOGO-10.png'></img>
            <span>نان</span>
          </a>
          <a>
            <img src='../../static/images/sections/LOGO-02.png'></img>
            <span>غذای گرم</span>
          </a>
          <a>
            <img src='../../static/images/sections/LOGO-01.png'></img>
            <span>عطاری</span>
          </a>
          <a>
            <img src='../../static/images/sections/LOGO-05.png'></img>
            <span>نوشیدنی</span>
          </a>
          <a>
            <img src='../../static/images/sections/LOGO-03.png'></img>
            <span>گوشت و ماهی</span>
          </a>
          <a>
            <img src='../../static/images/sections/LOGO-11.png'></img>
            <span>ترشی و لبنیات</span>
          </a>
          <a>
            <img src='../../static/images/sections/LOGO-09.png'></img>
            <span>میوه و سبزیجات</span>
          </a>
        </div>
      </SectionBody>
    </SectionWrapper>
  )
}

const SectionWrapper = styled.div`
  width: 100%;
  height: 230px;
`
const SectionBody = styled.div`
  width: 90%;
  height: auto;
  padding: 5px;
  margin: 0px auto;
  color: white;
  direction: rtl;
  div[class="boxes"] {
    width: 100%;
    height: auto;
    display: flex;
    flex-flow: row wrap;
    justify-content: space-between;
    a {
      width: 100px;
      height: 100px;
      display: block
      background: rgba(255, 255, 255, 0.8);
      border-radius: 15px;
      // margin-top: 10px;
      overflow: hidden;
      position: relative;
      img{
        display: block;
        width: 45px;
        height: 50px;
        margin: 10px auto;
      }
      span {
        width: 100%;
        height: 20px;
        display: block;
        font-size: 12px;
        text-align: center;
        position: absolute;
        bottom: 5px;
        color: #333;
      }
    }
  }
`
