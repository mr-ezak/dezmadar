import * as React from "react"
import styled from "styled-components"
import axios from "axios"
import Link from "next/link"
import Router from "next/router"
import { BarsIcon, PlusIcon, SearchIcon, CartIcon } from "./Icons"
import { MenuBar, NavBlank } from "./MenuBar"

export const Header = props => {
  const [right, setRight] = React.useState("-150px")
  const [display, setDisplay] = React.useState("none")
  const [searchtemp, setSearchtemp] = React.useState("none")
  const [searchWrited, setSearchWrited] = React.useState("")
  const [products, setProducts] = React.useState([])

  const handleNavClick = () => {
    setRight("0px")
    setDisplay("block")
  }

  const handleNavCloser = () => {
    setRight("-150px")
    setDisplay("none")
  }

  const quickSearch = async () => {
    const response = await axios.get(
      "https://base.bamadar.com/api/get_products?name=" + searchWrited
    )
    setProducts(response.data.data)
    setSearchtemp("block")
  }

  const moreSearch = () => {
    window.localStorage.setItem("searchWord", searchWrited)
    Router.push("/results")
  }

  React.useEffect(() => {
    if (searchWrited.length >= 2) {
      quickSearch()
    }
    if (searchWrited.length == 0) {
      setSearchtemp("none")
    }
  }, [searchWrited])

  const addToCart = data => {
    if (window.localStorage.getItem("MadarCart")) {
      let MadarArr = JSON.parse(localStorage.getItem("MadarCart"))
      let NewMadarArray = []
      let AddNew = true
      MadarArr.forEach((element, i) => {
        let pro = { ...element }
        if (element.id == data.id) {
          pro.balance = element.balance + 1
          AddNew = false
        }
        // console.log(pro)
        NewMadarArray.push(pro)
      })
      if (AddNew == true) {
        const pro = {
          id: data.id,
          balance: 1,
          name: data.name,
          price: data.sell_price
        }
        NewMadarArray.push(pro)
      }
      // console.log(NewMadarArray)
      window.localStorage.setItem("MadarCart", JSON.stringify(NewMadarArray))
      if (window.location.href == "http://localhost/cart") {
        Router.reload("/cart")
      }
    } else {
      const pro = {
        id: data.id,
        balance: 1,
        name: data.name,
        price: data.sell_price
      }
      const MadarArr = []
      MadarArr.push(pro)
      console.log(MadarArr)
      window.localStorage.setItem("MadarCart", JSON.stringify(MadarArr))
      if (window.location.href == "http://localhost/cart") {
        Router.reload("/cart")
      }
    }
  }

  return (
    <>
      <StyledHeader>
        <UperHeader>
          <p class='header-title'>هایپر مادر</p>
        </UperHeader>
        <DownerHeader>
          <HeaderBars onClick={() => handleNavClick()}>
            <BarsIcon />
          </HeaderBars>
          <HeaderSearchBox>
            <SearchIcon onClick={() => quickSearch()} />
            <input
              type='text'
              name='searchBox'
              placeholder='جستجو محصول'
              onChange={event => {
                setSearchWrited(event.target.value)
              }}
            />
            <HeaderSearchRes display={searchtemp}>
              <ul>
                {products.slice(0, 5).map((data, index) => (
                  <li key={index} id={data.id}>
                    <span>{data.name}</span>
                    <HeaderSearchAdd onClick={() => addToCart(data)}>
                      <PlusIcon />
                    </HeaderSearchAdd>
                  </li>
                ))}
                <HeaderSearchMore onClick={() => moreSearch()}>
                  موارد بیشتر
                </HeaderSearchMore>
              </ul>
            </HeaderSearchRes>
          </HeaderSearchBox>
          <HeaderCart>
            <Link href='/cart'>
              <CartIcon />
            </Link>
          </HeaderCart>
          {/* <div class='input-container'>
            <SearchInput name='search' type='text' right={this.state.right} />
          </div> */}
        </DownerHeader>
      </StyledHeader>
      <MenuBar right={right} />
      <NavBlank onClick={() => handleNavCloser()} display={display} />
    </>
  )
}

/* - - - Page Components - - - */

const StyledHeader = styled.div`
  width: 100%;
  height: 95px;
  background: #b05082;
  text-align: center;
  div[class="input-container"] {
    width: 200px;
    height: 40px;
    float: right;
    margin-right: 20px;
    margin-top: 20px;
    position: relative;
  }
  p[class="header-title"] {
    display: block;
    margin: 0px auto;
    width: 100px;
    height: 36px;
    line-height: 36px;
    color: #fff;
  }
`
const UperHeader = styled.div`
  width: 100%;
  height: 40px;
`
const DownerHeader = styled.div`
  width: 100%;
  height: 55px;
`
const HeaderCart = styled.div`
  width: 35px;
  height: 35px;
  float: left;
  margin-left: 15px;
  margin-top: 3px;
  cursor: pointer;
  svg {
    width: 100%;
    height: 100%;
  }
`
const HeaderSearchBox = styled.div`
  width: 65%;
  height: 40px;
  background: #eceff4;
  border-radius: 5px;
  box-shadow: 0px 0px 5px #999;
  margin-right: 5px;
  float: right;
  position: relative;
  input[name="searchBox"] {
    width: 80%;
    height: 30px;
    float: right;
    border: medium none;
    outline: medium none;
    padding: 0;
    padding-right: 5px;
    margin: 0;
    margin-top: 5px;
    margin-right: 5px;
    background: #eceff4;
    border-radius: 10px;
    direction: rtl;
    font-size: 11px;
    font-family: BYekan;
  }
  svg {
    float: right;
    margin-top: 10px;
  }
`
const HeaderSearchRes = styled.div`
  width: 100%;
  height: auto;
  display: ${p => (p.display ? p.display : "none")};
  min-height: 100px;
  background: rgba(255, 255, 255, 0.5);
  position: absolute;
  background: #efefef;
  border-radius: 5px;
  top: 40px;
  ul {
    list-style: none;
    li {
      width: 100%;
      height: 35px;
      line-height: 35px;
      font-size: 12px;
      border-bottom: 1px solid #ccc;
      text-align: right;
      padding-right: 3px;
    }
  }
`
const HeaderSearchAdd = styled.div`
  width: 30px;
  height: 30px;
  border-radius: 5px;
  float: left;
  margin-top: 1px;
  margin-left: 2px;
  background: #3498db;
  cursor: pointer;
  svg {
    display: block;
    width: 90%;
    height: 90%;
    margin: 3px auto;
  }
`
const HeaderSearchMore = styled.div`
  width: 120px;
  height: 30px;
  color: #333;
  font-size: 12px;
  border-radius: 5px;
  line-height: 30px;
  margin: 5px auto;
  background: rgba(52, 152, 219, 0.5);
  cursor: pointer;
`
const HeaderBars = styled.div`
  float: right;
  width: 50px;
  height: 50px;
  margin-right: 5px;
  cursor: pointer;
  svg {
    display: block;
    margin: 5px auto;
  }
`
