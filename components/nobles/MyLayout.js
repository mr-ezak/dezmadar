import styled from "styled-components"
import { Header } from "./Header"
import "../../static/css/style.css"

const Layout = props => (
  <Wrapper>
    <Header />
    <Content>
      {/* <img bg="unique" src="../../static/images/circle.png" /> */}
      {props.children}
    </Content>
  </Wrapper>
)

export default Layout

const Wrapper = styled.div`
  // overflow: hidden;
  margin: 0;
  padding: 0;
`
const Content = styled.div`
  width: 100%;
  height: auto;
  top: 95px;
  bottom: 0px;
  overflow: auto;
  position: absolute;
  background: #eceff4;
  z-index: -2;
  img[bg="unique"] {
    position: absolute;
    z-index: -1;
    width: 100%;
    margin-top: -5px;
  }
`
