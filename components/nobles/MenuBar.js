import React, { Component } from "react"
import styled from "styled-components"
import Link from "next/link"
import * as Icons from "./Icons"

export const MenuBar = props => {
  return (
    <NavWraper right={props.right}>
      <RawNavbar>
        <Link href='/'>
          <a>
            <Icons.HomeIcon />
            <span>صفحه نخست</span>
          </a>
        </Link>
        <Link href='/categories'>
          <a>
            <Icons.CategoriesIcon />
            <span>دسته بندی ها</span>
          </a>
        </Link>
        <Link href='/cart'>
          <a>
            <Icons.CartIcon />
            <span>سبد خرید</span>
          </a>
        </Link>
        <Link href='/location'>
          <a>
            <Icons.ServicesListIcon />
            <span>قوانین و مقررات</span>
          </a>
        </Link>
        <Link href='/location'>
          <a>
            <Icons.PrivacyIcon />
            <span>حریم خصوصی</span>
          </a>
        </Link>
        <Link href='/about'>
          <a>
            <Icons.AboutIcon />
            <span>درباره ما</span>
          </a>
        </Link>
        <Link href='/contact'>
          <a>
            <Icons.ContactIcon />
            <span>تماس با ما</span>
          </a>
        </Link>
      </RawNavbar>
    </NavWraper>
  )
}

export const NavBlank = props => {
  return <MainNavBlank display={props.display} {...props} />
}

/* - - - Styled Components - - - */

/* - - - Main Styled Components - - - */
const NavWraper = styled.div`
  width: 150px;
  height: 100%;
  position: fixed;
  top: 95px;
  right: ${d => (d.right ? d.right : "-150px")};
  bottom: 0px;
  z-index: 2;
  transition: all 300ms;
`

/* - - - RawNav Components - - - */

const RawNavbar = styled.div`
  width: 100%;
  height: 100%;
  overflow: hidden;
  a {
    display: block;
    width: 100%;
    height: 40px;
    color: #333;
    line-height: 30px;
    border-radius: 50px;
    cursor: pointer;
    text-decoration: none;
    margin-top: 5px;
    svg {
      width: 30px;
      height: 30px;
      float: right;
      display: block;
      margin-right: 10px;
    }
    span {
      width: 70%;
      height: 40px;
      float: left;
      display: block;
      text-align: right;
      font-size: 12px;
      color: #efefef;
    }
  }
`
const MainNavBlank = styled.div`
  position: absolute;
  width: 100%;
  height: 100%;
  background: rgba(0, 0, 0, 0.9);
  z-index: 1;
  top: 0;
  display: ${p => (p.display ? p.display : "none")};
  transition: all 300ms;
`
