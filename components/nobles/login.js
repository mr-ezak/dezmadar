import styled from "styled-components"
import { Component } from "react"
import axios from "axios"
import { postData } from "../../utils/main"
import Link from "next/link"
import Router from "next/router"

export default class Login extends Component {
  state = {
    mobile: "",
    code: "",
    page: "login"
  }

  loginReq = async () => {
    const mob = this.state.mobile
    const response = await axios.post(
      "https://base.dezmadar.com/api/user/login",
      {
        mobile: mob
      }
    )

    if (response.data.message == "EnterOTP") {
      this.setState({
        page: "not",
        mobile: mob
      })
    } else {
    }
  }

  verifyReq = async () => {
    const res = await axios.post("https://base.dezmadar.com/api/user/verify", {
      mobile: this.state.mobile,
      otp: this.state.code
    })

    console.log({ res }, res.data.data.access_token)
    if (({ res }, res.data.data.access_token)) {
      const token = ({ res }, res.data.data.access_token)
      window.localStorage.setItem("MadarLogged", 1)
      window.localStorage.setItem("access_token", token)
      window.location.reload()
    }
  }

  render = () => (
    <>
      {this.state.page == "login" ? (
        <LoginWrapper>
          <LoginLogo>
            <img src='/static/images/Logo.png' />
          </LoginLogo>
          <p align='center'>ورود</p>

          <form>
            <input
              type='text'
              placeholder='شماره موبایل'
              onChange={() => this.setState({ mobile: event.target.value })}
            />
            <LoginSubmitBTN onClick={this.loginReq}>ورود</LoginSubmitBTN>
          </form>
          {/* <LoginLoaging>در حال ارسال ...</LoginLoaging> */}
        </LoginWrapper>
      ) : (
        <LoginWrapper>
          <LoginLogo>
            <img src='/static/images/Logo.png' />
          </LoginLogo>
          <p align='center'>تایید</p>

          <form>
            <input
              type='text'
              placeholder='کد تاییدیه'
              onChange={() => this.setState({ code: event.target.value })}
            />
            <LoginSubmitBTN onClick={this.verifyReq}>تاییدیه</LoginSubmitBTN>
          </form>
        </LoginWrapper>
      )}
    </>
  )
}

const LoginLogo = styled.div`
  width: 100px;
  height: 100px;
  box-shadow: 0px 0px 10px #999;
  border-radius: 50px;
  margin: -60px auto;
  overflow: hidden;
  img {
    display: block;
    width: 100px;
    height: 100px;
  }
`
const LoginWrapper = styled.div`
  width: 250px;
  height: 300px;
  margin: 100px auto;
  background: #fff;
  border-radius: 15px;
  box-shadow: 0px 0px 5px #999;
  padding: 10px;
  form {
    width: 100%;
    height: 170px;
    display: block;
  }
  p {
    width: 100%;
    height: 20px;
    margin-top: 100px;
    color: #b05082;
    font-size: 18px;
  }
  form p {
    width: 100%;
    height: 40px;
    margin-top: 40px;
    direction: rtl;
    font-size: 15px;
    cursor: pointer;
    color: #b05082;
  }
  input {
    width: 100%;
    height: 35px;
    background: #fff;
    margin-top: 10px;
    padding-right: 5px;
    direction: rtl;
    border: none;
    border-bottom: 1px solid #999;
    box-sizing: border-box;
    font-family: BYekan;
  }
  input::placeholder {
    font-size: 14px;
    color: #666;
  }
  input:focus {
    border-bottom: 2px solid #b05082;
  }
`

const LoginSubmitBTN = styled.div`
  width: 100px;
  height: 40px;
  background-color: #b05082;
  border-radius: 10px;
  border: none;
  margin: 30px auto;
  color: white;
  font-family: BYekan;
  line-height: 40px;
  text-align: center;
  cursor: pointer;
  float: right;
`
const LoginLoaging = styled.div`
  width: 200px;
  height: 50px;
  background: #ccc;
  margin: 30px auto;
`
